Thing mqtt:topic:zigbee2mqtt  "MQTT - zigbee2mqtt"  (mqtt:broker:local) {
    Channels: Type switch : zigbeebridge_online "Online"  [ stateTopic = "zigbee2mqtt/bridge/state", off="offline", on="online"]
              Type string : zigbeebridge_info "Info"  [ stateTopic = "zigbee2mqtt/bridge/info"]
}

//Tradfri
Thing mqtt:topic:tradfri_remote_a "Tradfri Remote A" (mqtt:broker:local) {
    Channels: 
        Type string : button_press "button press" [
            stateTopic="zigbee2mqtt/tradfri_remote",
            transformationPattern="JSONPATH:$.action"
         ]
}

Thing mqtt:topic:tradfri_switch_a "Tradfri Switch A" (mqtt:broker:local) {
    Channels: 
        Type string : action "action" [
            stateTopic="zigbee2mqtt/tradfri_switch_a",
            transformationPattern="JSONPATH:$.action"
         ]
}

// Lampen
Thing mqtt:topic:light_livingroom "Lampe Wohnzimmer" (mqtt:broker:local) {
    Channels: 
        Type dimmer : brightness  "Helligkeit" [
            stateTopic="zigbee2mqtt/light_livingroom/brightness",
            commandTopic="zigbee2mqtt/light_livingroom/set/brightness",
            min=0,
            max=255,
            step=1
        ]
}

// Steckdosen
Thing mqtt:topic:plug_a "Steckdose A" (mqtt:broker:local) {
    Channels: 
        Type switch : state "State" [
            commandTopic="zigbee2mqtt/plug_a/set", on="ON", off="OFF",
            stateTopic="zigbee2mqtt/plug_a"
         ]
         Type number : linkquality "Verbindungsqualität" [
            stateTopic="zigbee2mqtt/plug_a",
            transformationPattern="JSONPATH:$.linkquality"
         ]
}

Thing mqtt:topic:plug_b "Steckdose B" (mqtt:broker:local) {
    Channels: 
        Type switch : state "State" [
            commandTopic="zigbee2mqtt/plug_b/set", on="ON", off="OFF",
            stateTopic="zigbee2mqtt/plug_b"
         ]
         Type number : linkquality "Verbindungsqualität" [
            stateTopic="zigbee2mqtt/plug_b",
            transformationPattern="JSONPATH:$.linkquality"
         ]
}

Thing mqtt:topic:plug_c "Steckdose C" (mqtt:broker:local) {
    Channels: 
        Type switch : state "State" [
            commandTopic="zigbee2mqtt/plug_c/set", on="ON", off="OFF",
            stateTopic="zigbee2mqtt/plug_c"
         ]
         Type number : linkquality "Verbindungsqualität" [
            stateTopic="zigbee2mqtt/plug_c",
            transformationPattern="JSONPATH:$.linkquality"
         ]
}

// Temperature Sensors
Thing mqtt:topic:temp_a "Temperatursensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/temp_a",
            transformationPattern="JSONPATH:$.battery"
         ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/temp_a",
            transformationPattern="JSONPATH:$.linkquality"
         ] 
        Type number : temperature "Temperatur" [
            stateTopic="zigbee2mqtt/temp_a",
            transformationPattern="JSONPATH:$.temperature"
         ]
        Type number : humidity "Luftfeuchtigkeit" [
            stateTopic="zigbee2mqtt/temp_a",
            transformationPattern="JSONPATH:$.humidity"
         ]
        Type number : pressure "Luftdruck" [
            stateTopic="zigbee2mqtt/temp_a",
            transformationPattern="JSONPATH:$.pressure"
         ]
}

Thing mqtt:topic:temp_b "Temperatursensor B" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/temp_b",
            transformationPattern="JSONPATH:$.battery"
         ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/temp_b",
            transformationPattern="JSONPATH:$.linkquality"
         ] 
        Type number : temperature "Temperatur" [
            stateTopic="zigbee2mqtt/temp_b",
            transformationPattern="JSONPATH:$.temperature"
         ]
        Type number : humidity "Luftfeuchtigkeit" [
            stateTopic="zigbee2mqtt/temp_b",
            transformationPattern="JSONPATH:$.humidity"
         ]
        Type number : pressure "Luftdruck" [
            stateTopic="zigbee2mqtt/temp_b",
            transformationPattern="JSONPATH:$.pressure"
         ]
}

Thing mqtt:topic:temp_c "Temperatursensor B" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/temp_c",
            transformationPattern="JSONPATH:$.battery"
         ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/temp_c",
            transformationPattern="JSONPATH:$.linkquality"
         ] 
        Type number : temperature "Temperatur" [
            stateTopic="zigbee2mqtt/temp_c",
            transformationPattern="JSONPATH:$.temperature"
         ]
        Type number : humidity "Luftfeuchtigkeit" [
            stateTopic="zigbee2mqtt/temp_c",
            transformationPattern="JSONPATH:$.humidity"
         ]
        Type number : pressure "Luftdruck" [
            stateTopic="zigbee2mqtt/temp_c",
            transformationPattern="JSONPATH:$.pressure"
         ]
}

Thing mqtt:topic:temp_d "Temperatursensor B" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/temp_d",
            transformationPattern="JSONPATH:$.battery"
         ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/temp_d",
            transformationPattern="JSONPATH:$.linkquality"
         ] 
        Type number : temperature "Temperatur" [
            stateTopic="zigbee2mqtt/temp_d",
            transformationPattern="JSONPATH:$.temperature"
         ]
        Type number : humidity "Luftfeuchtigkeit" [
            stateTopic="zigbee2mqtt/temp_d",
            transformationPattern="JSONPATH:$.humidity"
         ]
        Type number : pressure "Luftdruck" [
            stateTopic="zigbee2mqtt/temp_d",
            transformationPattern="JSONPATH:$.pressure"
         ]
}

// Door Sensors

Thing mqtt:topic:door_a "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/door_a",
            transformationPattern="JSONPATH:$.battery"
        ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/door_a",
            transformationPattern="JSONPATH:$.linkquality"
        ] 
        Type string : contact "Kontakt" [
            stateTopic="zigbee2mqtt/door_a",
            transformationPattern="JSONPATH:$.contact"
        ]
}

Thing mqtt:topic:door_b "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/door_b",
            transformationPattern="JSONPATH:$.battery"
        ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/door_b",
            transformationPattern="JSONPATH:$.linkquality"
        ] 
        Type string : contact "Kontakt" [
            stateTopic="zigbee2mqtt/door_b",
            transformationPattern="JSONPATH:$.contact"
        ]
}

Thing mqtt:topic:door_c "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/door_c",
            transformationPattern="JSONPATH:$.battery"
        ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/door_c",
            transformationPattern="JSONPATH:$.linkquality"
        ] 
        Type string : contact "Kontakt" [
            stateTopic="zigbee2mqtt/door_c",
            transformationPattern="JSONPATH:$.contact"
        ]
}

Thing mqtt:topic:door_d "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/door_d",
            transformationPattern="JSONPATH:$.battery"
        ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/door_d",
            transformationPattern="JSONPATH:$.linkquality"
        ] 
        Type string : contact "Kontakt" [
            stateTopic="zigbee2mqtt/door_d",
            transformationPattern="JSONPATH:$.contact"
        ]
}

Thing mqtt:topic:door_e "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type number : battery "Batterie" [
            stateTopic="zigbee2mqtt/door_e",
            transformationPattern="JSONPATH:$.battery"
        ]
        Type number : linkquality "Verbindungsqualitaet" [
            stateTopic="zigbee2mqtt/door_e",
            transformationPattern="JSONPATH:$.linkquality"
        ] 
        Type string : contact "Kontakt" [
            stateTopic="zigbee2mqtt/door_e",
            transformationPattern="JSONPATH:$.contact"
        ]
}

// LED
Thing mqtt:topic:led_schreibtisch "Doorsensor A" (mqtt:broker:local) {
    Channels: 
        Type color : color "Farbe" [
            commandTopic="zigbee2mqtt/led_schreibtisch/set",
            color_mode="xyY",
            stateTopic="zigbee2mqtt/led_schreibtisch",
            transformationPatternOut="JS:color2zigbee.js"
        ]
        Type number : brightness_rgb "Farbhelligkeit" [
            commandTopic="zigbee2mqtt/led_schreibtisch/set",
            stateTopic="zigbee2mqtt/led_schreibtisch",
            transformationPattern="JSONPATH:$.brightness_rgb",
            transformationPattern="JS:zigbee2rgb_brightness.js",
            transformationPatternOut="JS:rgb_brightness2zigbee.js"
        ]
        Type number : brightness "Helligkeit" [
            commandTopic="zigbee2mqtt/led_schreibtisch/set",
            transformationPattern="JSONPATH:$.brightness_white",
            stateTopic="zigbee2mqtt/led_schreibtisch",
            transformationPattern="JS:zigbee2brightness.js",
            transformationPatternOut="JS:brightness2zigbee.js"
        ]                
}