(function(i) {
    var result;
    var json = JSON.parse(i);
    if (json.brightness_white == 0) {
        return 0;
    }    
    if (json.brightness_white == 254) {
        return 100;
    } 
    result = Math.round(((json.brightness_white / 255) * 100));
    return result;
})(input)
